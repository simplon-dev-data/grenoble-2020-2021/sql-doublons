# Activité gestion des doublons en SQL

> Comment automatiser un processus sans doublons de données ?

Un problème récurrent avec les _pipelines de données_ est la gestion des doublons.
Comment faire pour automatiser un pipeline pour récupérer des données source
et les intégrer dans ma base de données analytique sans obtenir des doublons
à chaque itération ?

Un processus de traitement de données qui est capable de gérer les doublons
est appelé **idempotent** : cela veut dire qu'on peut exécuter le pipeline
autant de fois que l'on désire et **le résultat sera toujours le même si les données sources n'ont pas changées**.

Nous allons voir plusieurs méthodes en langage SQL, en utilisant le SGBDR PostgreSQL.

## Mise en place

Un fichier de configuration [docker-compose.yml](docker-compose.yml) permet
d'instancier le serveur PostgreSQL :

```bash
sudo docker-compose up -d
```

Pour se connecter au serveur PostgreSQL :

```bash
sudo docker-compose exec postgres psql -h postgres -U postgres doublons
```

## Jeu de données

Afin de pouvoir expérimenter les techniques de gestion des doublons,
nous allons utiliser un jeu de données au format CSV en 2 parties :

- Les données initiales : [data_initial.csv](data_initial.csv)
- Les données mises à jour : [data_update.csv](data_update.csv)

Les données mises à jour contiennent à la fois certaines des données initiales
ainsi que de nouvelles données. Le but du jeu étant de :

1. Charger les données initiales en base
2. Charger les données mises à jour
3. Vérifier qu'aucun doublon ne reste
4. Retour au point 2

Pour pouvoir charger les jeux de données dans PostgreSQL avec la commande `COPY`,
n'oubliez pas qu'il faut copier les jeux de données dans le dossier `.docker/data` :

```bash
sudo cp data_initial.csv .docker/data/
sudo cp data_update.csv .docker/data/
```

Pour charger les données intiales :

```sql
CREATE TABLE IF NOT EXISTS raw_data (
    "Date" VARCHAR(16),
    "HeureDebut" TIME,
    "HeureFin" TIME,
    "Date et Heure" TIMESTAMP WITH TIME ZONE,
    "Filiere" VARCHAR(16),
    "Energie France (MWh)" INTEGER
);

CREATE TABLE IF NOT EXISTS clean_data (
    "Date" VARCHAR(16),
    "HeureDebut" TIME,
    "HeureFin" TIME,
    "Date et Heure" TIMESTAMP WITH TIME ZONE,
    "Filiere" VARCHAR(16),
    "Energie France (MWh)" INTEGER
);

COPY raw_data
FROM '/data/data_initial.csv' (FORMAT 'csv', DELIMITER ';', HEADER TRUE);

INSERT INTO clean_data
SELECT *
FROM raw_data;
```

Pour charger les données mises à jour contenant les doublons :

```sql
TRUNCATE TABLE raw_data;

COPY raw_data
FROM '/data/data_update.csv' (FORMAT 'csv', DELIMITER ';', HEADER TRUE);

INSERT INTO clean_data
SELECT *
FROM raw_data;
```

Pour observer les doublons dans la table unifiée :

```sql
SELECT "Date et Heure", "Filiere", "Energie France (MWh)", COUNT(*)
FROM clean_data
GROUP BY "Date et Heure", "Filiere", "Energie France (MWh)"
HAVING  COUNT(*) > 1
ORDER BY COUNT(*) DESC;
```

On obtient les doublons suivants :

| Date et Heure          | Filiere | Energie France (MWh) | count |
| ---------------------- | ------- | -------------------- | ----- |
| 2020-10-31 07:00:00+00 | Solaire | 520                  | 2     |
| 2020-10-30 17:00:00+00 | Solaire | 6                    | 2     |
| 2020-10-30 20:00:00+00 | Solaire | 1                    | 2     |
| 2020-10-31 14:00:00+00 | Solaire | 1655                 | 2     |
| 2020-10-30 21:00:00+00 | Solaire | 1                    | 2     |
| 2020-10-31 12:00:00+00 | Solaire | 2781                 | 2     |

## Méthodes

Il existe plusieurs méthodes pour gérer les doublons avec PostgreSQL basées
sur les opérateurs suivants :

- `TRUNCATE`
- `WHERE ... NOT IN ...`
- `WHERE NOT EXISTS ...`
- `ON CONFLICT ... DO ...`
- `UNIQUE`

Chaque opérateur a ses avantages et inconvénients. En revanche, tous les
opérateurs nécessitent de réfléchir aux condition de détermination
d'un doublon (autrement dit, quelle(s) donnée(s) permet(tent) de décider si une ligne est un doublon) !

Effectuez vos recherches et testez chaque opérateur pour comprendre
leur utilité.

**À vous de jouer !**

## Exemples de solutions

Avant tout, il est nécessaire de considérer les colonnes qui permettent
de déterminer un doublon dans notre jeu de données :

- `"Date et Heure"`
- `"Filiere"`
- `"Energie France (MWh)"`

La combinaison des valeurs de ces 3 colonnes permet d'établir que
2 lignes sont identiques.

### `WHERE ... NOT IN ...`

Cette construction tente de "matcher" les lignes de la table de départ
dont la combinaison de colonne existe déjà dans la table d'arrivée.
Les lignes qui sont déjà présentent dans à l'arrivée ne sont pas insérées
une seconde fois :

```sql
INSERT INTO clean_data
SELECT *
FROM raw_data
WHERE ("Date et Heure", "Filiere", "Energie France (MWh)") NOT IN (
    SELECT "Date et Heure", "Filiere", "Energie France (MWh)"
    FROM clean_data
);
```

### `WHERE NOT EXISTS ...`

Cette construction retourne `TRUE` quand le nombre de ligne d'une sous-requête
est égal à zéro, `FALSE` sinon.

Donc on utilise cet opérateur pour sélectionner
toutes les lignes qui seraient déjà présentes dans la table d'arrivée et on
condition sur le nombre de lignes en doublon :

```sql
INSERT INTO clean_data
SELECT *
FROM raw_data
WHERE NOT EXISTS (
    SELECT "Date et Heure", "Filiere", "Energie France (MWh)"
    FROM clean_data
    WHERE
        "Date et Heure" = raw_data."Date et Heure"
        AND "Filiere" = raw_data."Filiere"
        AND "Energie France (MWh)" = raw_data."Energie France (MWh)"
);
```

### `ON CONFLICT ... DO ...`

Avec cette construction, il est nécessaire de modifier la structure de la table
`clean_data` pour ajouter une **contrainte d'unicité**sur les colonnes constituant
un doublon :

```sql
CREATE TABLE IF NOT EXISTS clean_data (
    "Date" VARCHAR(16),
    "HeureDebut" TIME,
    "HeureFin" TIME,
    "Date et Heure" TIMESTAMP WITH TIME ZONE,
    "Filiere" VARCHAR(16),
    "Energie France (MWh)" INTEGER,
    UNIQUE ("Date et Heure", "Filiere", "Energie France (MWh)")
);
```

On peut ensuite utiliser la construction pour ne pas faire d'action dès qu'une
ligne à insérer "viole" la contrainte d'unicité :

```sql
INSERT INTO clean_data
SELECT *
FROM raw_data
ON CONFLICT ("Date et Heure", "Filiere", "Energie France (MWh)") DO NOTHING;
```

_Remarque_ : ce type de construction permet également de mettre à jour des lignes
déjà présente, afin de rafraichir les données propres en base (utile dans certains
cas bien précis). Ce type d'opération s'appele un **upsert** (_update and insert_).
